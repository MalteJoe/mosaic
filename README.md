# README #

WIP of an image mosaic filter

## Prerequisites (Ubuntu)
* libopencv-dev
* libhighgui-dev

## TODO/Ideas
* Number of vertices shouldn't be attached to image size
* Blur the result of edge detection to allow more points around edges
* specify output folder / filename
* Calculate LOD automatically by looking at complexity of image (few edges = dark edge map -> low LOD)
* Make options configurable via args