/*
 * main.cpp
 *
 *  Created on: 09.07.2017
 *      Author: malte
 */

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;

#define OUTPUT_FOLDER string("./")
#define LEVEL_OF_DETAIL 3

/**
 * Sample random points on the input image.
 * Points closer to edges are preferred.
 * @param img Input image
 * @return A list of points
 */
vector<cv::Point> createPoints(const cv::Mat& img) {
    vector<cv::Point> points;
    cv::Mat sobelImg = img.clone();

    // Find edges
    cv::Sobel(img, sobelImg, -1, 1, 1);
    cv::cvtColor(sobelImg, sobelImg, CV_BGR2GRAY);
#ifdef DEBUG
    cv::imshow("Edge detection", sobelImg);
    cv::waitKey();
#endif
    // Points don't need to be exactly on the edges, so blur them a little
    cv::GaussianBlur(sobelImg, sobelImg, cv::Size(5, 5), 5);
#ifdef DEBUG
    cv::imshow("Edge detection", sobelImg);
    cv::waitKey();
#endif
    // normalize it so we don't reject as many points
    cv::normalize(sobelImg, sobelImg, 0, 255, cv::NORM_MINMAX);
#ifdef DEBUG
    cv::imshow("Edge detection", sobelImg);
    cv::waitKey();
#endif
    const size_t pointsToGenerate = LEVEL_OF_DETAIL * LEVEL_OF_DETAIL * 500;
    while (points.size() < pointsToGenerate) {
        // random point on image
        cv::Point p(rand() % sobelImg.size().width,
                rand() % sobelImg.size().height);
        // only push it to the list, if we have a high intensity
        // of the edge-detection
        if (rand() % 255 <= sobelImg.at<uchar>(p))
            points.push_back(p);
    }
    return points;
}

/**
 * For now accepts only filenames to process as arguments
 */
int main(int argc, char **argv) {
    if (argc < 2) {
        cerr << "No image specified!" << endl;
    }
    srand(time(0));
    int errorCount = 0;

    // for all files
    while (argc-- > 1) {
        string filepath = argv[argc];
        string filename = basename(argv[argc]);
        // Read in image
        cv::Mat img = cv::imread(filepath, cv::IMREAD_COLOR);
        if (!img.data) {
            cerr << "Failed to load image \"" << filepath << "\"" << endl;
            errorCount++;
        } else {
            // Sample points on edges
            std::vector<cv::Point> points = createPoints(img);

#ifdef DEBUG
            {
                cv::Mat copy = img.clone();
                for (const cv::Point& p : points) {
                    circle(copy, p, 1, cv::Scalar(0, 0, 255), CV_FILLED, CV_AA);
                }
                cv::imshow("Sampled Points", copy);
                cv::waitKey();
            }
#endif

            // (Let OpenCV) calculate Delaunay triangulation
            cv::Rect canvas(0, 0, img.size().width, img.size().height);
            cv::Subdiv2D subdiv(canvas);
            for (const cv::Point& p : points) {
                subdiv.insert(p);
            }
            vector<cv::Vec6f> triangles;
            subdiv.getTriangleList(triangles);

#ifdef DEBUG
            {
                cv::Mat copy = img.clone();
                vector<cv::Point> pt(3);
                for (const cv::Vec6f& t : triangles) {
                    pt[0] = cv::Point(cvRound(t[0]), cvRound(t[1]));
                    pt[1] = cv::Point(cvRound(t[2]), cvRound(t[3]));
                    pt[2] = cv::Point(cvRound(t[4]), cvRound(t[5]));

                    cv::line(copy, pt[0], pt[1], cv::Scalar(), 1);
                    cv::line(copy, pt[1], pt[2], cv::Scalar(), 1);
                    cv::line(copy, pt[2], pt[0], cv::Scalar(), 1);
                }
                cv::imshow("Delaunay Triangles", copy);
                cv::waitKey();
            }
#endif

            // create a new image to draw onto and paint it black.
            cv::Mat out = img.clone();
            cv::rectangle(out, canvas, cv::Scalar(), CV_FILLED);

            // Process each triangle
            vector<cv::Point> vertices(3);
            cv::Scalar color;
            for (const cv::Vec6f& t : triangles) {
                float sumX = 0, sumY = 0;
                // convert that weird Vec6f into three nice vertices
                for (size_t i = 0; i < 3; i++) {
                    vertices[i] = cv::Point(t[i * 2], t[i * 2 + 1]);
                    sumX += t[i * 2];
                    sumY += t[i * 2 + 1];
                }
                // don't process triangles whose vertices are all outside
                // the canvas
                if (canvas.contains(vertices[0]) || canvas.contains(vertices[1])
                        || canvas.contains(vertices[2])) {
                    // center of the triangle to get the color from
                    cv::Point center(sumX / 3, sumY / 3);

                    // if the triangle is only partially on the canvas, the
                    // center might be outside
                    if (!canvas.contains(center)) {
                        vector<cv::Point> inside;
                        // consider all points which are on the canvas
                        for (size_t i = 0; i < 3; i++) {
                            if (canvas.contains(vertices[i])) {
                                inside.push_back(vertices[i]);
                            }
                        }

                        // create a new line (if one vertex is inside the canvas)
                        // or triangle (if two vertices are inside the canvas)
                        // and move the point from which we will take the color
                        // to it's center. We do this until the point is inside
                        // the canvas.
                        do {
                            for (const cv::Point& p : inside) {
                                center += p;
                            }
                            // +0.5f because
                            // -1 / 2 == -1
                            // on my machine for some reason
                            center.x = (center.x + 0.5f) / (inside.size() + 1);
                            center.y = (center.y + 0.5f) / (inside.size() + 1);
                        } while (!canvas.contains(center));
                    }
                    // draw the triangle with the color obtained from the center
                    cv::fillConvexPoly(out, vertices,
                            cv::Scalar(img.at<cv::Vec3b>(center)), CV_AA);
                }
            }

            // Save image
            if (!cv::imwrite(OUTPUT_FOLDER + filename, out)) {
                cerr << "Fehler beim Speichern von Bild \"" << filename << "\""
                        << endl;
                errorCount++;
            }
#ifdef DEBUG
            else {
                cv::imshow("Result", out);
                cv::waitKey();
            }
#endif
        }
    }

    return errorCount;
}
